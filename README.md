# VCM Presentation Repository

This repository contains files for my presentation on time-varying coefficient models.

## GSS

The GSS directory contains the presentation for the Graduate Student Seminar presented on April 21, 2020.

## UCM

The UCM directory contains the presentation for UC Merced's SAMPLe Seminar presented on October 16, 2020.